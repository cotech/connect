# CoTech Connect
## Communication Launchpad

Result of [June 2022 gathering in Birmingham](https://wiki.coops.tech/wiki/June_Gathering_2022).

### Stack
This project uses [Vue.js 3](https://v3.vuejs.org/) with its [routing](https://v3.vuejs.org/guide/routing.html) for moving between views with a [single-page application](https://en.wikipedia.org/wiki/Single-page_application) feel, and [Vuex](https://next.vuex.vuejs.org/), allowing for dynamic information retrieval and storage within the tab session.
[Tailwind CSS](https://tailwindcss.com/) covers styling. Tailwind added not via npm but using [Vue CLI](https://cli.vuejs.org/) `vue add tailwind`, 'minimal' option.

### Project setup

#### Install npm modules
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Lints and fixes files
```
npm run lint
```

#### Compiles and minifies for production
```
npm run build
```
*When copying dist onto the server need to include .htaccess for router to work with [our server setup](https://router.vuejs.org/guide/essentials/history-mode.html#apache).*



### TODO 
- Text content - edits & more actions/questions - src/store/index.js
- Imagery - gif per channel?
- Styling - Tailwind?
- Collect feedback!
- Update dependencies.

### Further work
- Nested questions for more contextualised paths.